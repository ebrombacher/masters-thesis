
source("functions.R")

getCorrectProportions <- function(cm, proportionMode){
  if (proportionMode != "accGenus"){
    cm<-cm[!(cm$Prediction==cm$Reference),] # comment out for accuracy on genus level
  }
  cm$Reference = sub("_.*", "", cm$Reference)
  cm$Prediction = sub("_.*", "", cm$Prediction)
  sum <- sum(cm$Freq)
  same<-sum(cm[cm$Prediction==cm$Reference,]$Freq)
  sameGenus_prop <- same/sum
  return(sameGenus_prop)
}


getTableSortedByRowsAndCols <- function(table){
  table <- table[,sort(names(table))] 
  table <- table[ order(row.names(table)), ]
  return(table)
}


# Return reduced version of in silico database "df_database" with only the classes that can also be found in experimental dataset
getInsilicoExpIntersect <- function(df_database, level, genusPredMode){
  # get experimental data
  df <- getRobertKoch()
  df_spectrabank <- getSpectraBank()
  df_embarc <- getEmbarc()
  df_bacteriams <- getBacteriaMS()
  df_spectrasweden <- getSpectraSweden()
  library(qpcR)
  exp_df <- qpcR:::cbind.na(df, df_spectrabank, df_embarc, df_bacteriams, df_spectrasweden)
  if (level=="species" || genusPredMode==2){
    colnames(exp_df) <- gsub("^([^_]*_[^_]*)_.*$", "\\1", colnames(exp_df))
  } else if (level=="genus"){
    colnames(exp_df) <- sub("_.*", "", colnames(exp_df))
    
  } 
  colnames(exp_df) <- gsub("\\..*","", colnames(exp_df))
  #write.csv(exp_df, file = "expDataframes_combined.csv", row.names = FALSE)
  # remove row in silico database if df_database$class is not in experimental database
  df_database<-df_database[df_database$class %in% colnames(exp_df),]
  
  return(list(df_database, exp_df))
}

# Make predictions of the identity the bacterium to which the query belongs has
generateMap <- function(query, sigma, level, precision, genusPredMode, withPTMs,penalty,combineExp=FALSE){
  if (withPTMs==1){
    if (genusPredMode==2){
      path <- paste("naiveBayes_species_",precision, ".csv",  sep="")
    } else {
      path <- paste("naiveBayes_",level,"_",precision, ".csv",  sep="")
    }
    df_database <- read.csv(path,header=T)
  } else if (withPTMs==0){
    if (genusPredMode==2){
      path <- paste("naiveBayes_species_",precision,"_PTMsRemoved.csv",  sep="")
    } else {
      path <- paste("naiveBayes_",level,"_",precision,"_PTMsRemoved.csv",  sep="")    
    }
    df_database <- read.csv(path,header=T)
  } else if(withPTMs==2){
    if (genusPredMode==2){
      path <- paste("naiveBayes_species_",precision, ".csv",  sep="")
      path2 <- paste("naiveBayes_species_",precision,"_PTMsRemoved.csv",  sep="")
    } else {
      path <- paste("naiveBayes_",level,"_",precision, ".csv",  sep="")
      path2 <- paste("naiveBayes_",level,"_",precision,"_PTMsRemoved.csv",  sep="")
    }
       
    df_database <- rbind(read.csv(path,header=T), read.csv(path2,header=T))
    
    # Remove duplicate rows resulting from combining the data frames
    df_database <- unique(df_database)
  }
  
  if (combineExp){
    expIntersect <- getInsilicoExpIntersect(df_database, level, genusPredMode)
    df_database <- expIntersect[[1]]
    exp_df <- expIntersect[[2]]
  }
  
  df_database$keep <- FALSE
  
  map <- data.frame(matrix(nrow = length(unique(df_database$class)), ncol = length(unique(df_database$protein))))
  colnames(map) <- unique(df_database$protein)
  rownames(map) <- unique(df_database$class)
  
  none <- 6.049268112978591*10^(-6)
  
  map[is.na(map)] <- none
  
  for (p in query){
    df_database$keep[(df_database$mean<=(p+sigma))&(df_database$mean>=(p-sigma))]=TRUE
  }
  
  df_database <- df_database[df_database$keep==TRUE,]
  
  # If any entry is left after pre-selection
  if (nrow(df_database)>0){
    masses  <- rev(expand.grid(list(bacterium=unique(df_database$class),proteins=unique(df_database$protein))))
    namevector <- c("mass", "query_mass", "probab")
    masses[ , namevector] <- NA
    
    if (nrow(df_database)>0){
      df_database$keep <- NULL
      
      for (p in query){
        # go through each row of db 
        for(i in 1:nrow(df_database)) { 
          
          #get species, protein, std_dev, mean
          class <- df_database[i,1]
          protein <- df_database[i,2]
          std_dev <- df_database[i,4]
          mean <- df_database[i,6]
          
          # calculate probability
          new <- probab(p, std_dev, mean)
          if (combineExp){
            # check if mass "p" occurs for value of "class" in any entry of the respective class of the experimental datasets with a
            # tolerance of 10 Da and penalize with 20% reduction of "new" if not
            if(class %in% colnames(exp_df)){
              idx <- which(colnames(exp_df)==class)
              found = FALSE
              for (index in idx){
                expMasses <- as.vector(na.omit(exp_df[,index]))
                for (m in expMasses){
                  tol <- 30 # Instead of fixed value 0.0015>= abs(p-m)/p corresponding to 1500 ppm???
                  if (abs(m-p)<tol){
                  #if ( 0.002>= abs(p-m)/p){
                    found = TRUE # Fire the flag, and break the inner loop
                    break
                  }
                }
                if (found){break}
              }
              #if (!found) {new <- penalty*new}
              if (found) {new <- penalty*new}
            }
          }
          if(new>map[as.character(class), as.character(protein)]){
            map[as.character(class), as.character(protein)] <- new
            masses$mass[masses$bacterium==as.character(class)&masses$proteins==as.character(protein)] <- mean
            masses$query_mass[masses$bacterium==as.character(class)&masses$proteins==as.character(protein)] <- p
            masses$probab[masses$bacterium==as.character(class)&masses$proteins==as.character(protein)] <- new
            #print(as.character(class))
          }
        }
      }
    }  
    map <- transform(map, prod=Reduce(`*`, map))
    
    masses <- masses[!is.na(masses$mass),]
    
    # If multiple rows with same bacterium and query_mass keep row with highest value of probab
    masses <- masses[order(masses$bacterium, masses$query_mass, -abs(masses$probab) ), ] ### sort first
    masses <- masses[!duplicated(masses[,c("bacterium","query_mass")]),]
    masses$probab <- NULL
    
    masses <- masses[with(masses, order(bacterium, mass)),]
    masses <- data.frame(lapply(masses, as.character), stringsAsFactors=FALSE)
    
    masses <- aggregate(. ~ bacterium, data = masses, paste, collapse = ",")
    
    if (level=="species" || genusPredMode==2){
      freq <- read.csv("kegg_species_frequencies.csv")
      freq$bacterium <- gsub("\\ ", "_", freq$bacterium)
    } else if (level=="genus"){
      freq <- read.csv("kegg_genus_frequencies.csv")
    } 
    
    # total no. of samples
    total <- sum(freq$frequency)
    freq$proportion <- freq$frequency/total
    map$bacterium <- rownames(map)
    
    library(plyr)
    final_map <- join(map,freq,by='bacterium')
    final_map$result <- final_map$prod*final_map$proportion
    
    
    final_map <- join(final_map,masses,by='bacterium')
    final_map <- final_map[order(-final_map$result),] 
    
    return(final_map)
  } else {
    return(FALSE)
  }
}

# Get confusion matrix for the specified settings applied on an experimental datatset
getConfMatrix <- function(test, sigma, level, precision, method, matrix, genusPredMode,withPTMs, penalty=1, combineExp, queryModif="none"){
 if (level=="species"|| genusPredMode!=0){
    freq <- read.csv("kegg_species_frequencies.csv")
    freq$bacterium <- gsub("\\ ", "_", freq$bacterium)
    colnames(test) <- gsub("^([^_]*_[^_]*)_.*$", "\\1", colnames(test))
  } else if (level=="genus"){
    freq <- read.csv("kegg_genus_frequencies.csv")
    colnames(test) <- sub("_.*", "", colnames(test))
  }
  
  colnames(test) <- gsub("\\..*","", colnames(test))
  
  intersect <- intersect(unique(freq$bacterium), unique(colnames(test)))
  test <- test[ ,colnames(test) %in% intersect]
  if(level == "genus" && genusPredMode==1){
    colnames(test) <- sub("_.*", "", colnames(test))
  }
  names <- colnames(test)
  names <- gsub("\\..*","",names)
  
  numQueries <- ncol(test)
  
  if (method == "bins"){
    insilico_bins <- generateBinsInSilicoTable(level,precision)  
    keep <- colnames(insilico_bins[1:(length(insilico_bins)-1)])  
  }

  true <- c()
  prediction <- c()
  
  for (i in 1:numQueries){
    trueClass <- names[i]
    if(genusPredMode==2){
      trueClass <- sub("_.*", "", trueClass)
    }
    true <- c(true, trueClass)
    query <- test[,i]
    query <- query[!is.na(query)]
    
    if (queryModif == "doubleAdded"){
      query <- c(query, 2*query) 
    } else if (queryModif == "replacedByDouble"){
      query <- 2*query 
    }
    
    if (method == "map"){
      map <- generateMap(query, sigma, level, precision, genusPredMode, withPTMs, penalty,combineExp) # for including experimental data
      
      if (length(map)>1){
        max <- map$bacterium[which.max(map$result)]
        if(genusPredMode==2){
          max <- sub("_.*", "", max)
        }
      } else {
        max <- "none"
      }
    } else if (method == "bins"){
      query_bins <- generateQueryBins(query, level, keep)
      if (sum(query_bins)>0){
        bins_result <- getBinsResult(insilico_bins, query_bins)
        max <- as.vector(bins_result$rank[which.max(bins_result$similarity)])
      } else {
        max <- "none"
      }
    }
    #print(max)
    prediction <- c(prediction, max)
  }
  
  if (matrix == TRUE){
    library(caret)
    lvs <- union(prediction, true)
    prediction <- factor(prediction,levels = lvs)
    true <- factor(true,levels = lvs)
    conf <- caret::confusionMatrix(prediction,true, mode="everything")
    
    return(conf)
  } else {
    return(prediction)
  }
}

# Combine the results of the different setting combinations
combineConfs <- function(i, confs, result_df, db,  sigma, level, precision, method, matrix, genusPredMode, withPTM, penalty, combineExp, queryModif){
  conf <- getConfMatrix(db,  sigma, level, precision, method, TRUE, genusPredMode, withPTM, penalty, combineExp, queryModif)
  confs[[i]] <- conf
  measures <- getPerformanceMeasures(conf)
  result_df[i,] <- c(precision, method, level, queryModif, genusPredMode, withPTM, combineExp, penalty, measures[[1]],measures[[2]],measures[[3]], measures[[4]], measures[[5]], measures[[6]], measures[[7]], measures[[8]], measures[[9]], measures[[10]])
  return(list(confs,result_df))
}

confs <- vector("list")
accs <- vector("list")

sigma <- 3
method <- "map" 
#precisions <- c("precision1", "standardPrecision")
precisions <- c("precision1")

#db <- getPineda()
#db <- getPinedaSa()

df <- getRobertKoch()
df_spectrabank <- getSpectraBank()
df_embarc <- getEmbarc()
df_bacteriams <- getBacteriaMS()
df_spectrasweden <- getSpectraSweden()
library(qpcR)
db <- qpcR:::cbind.na(df, df_spectrabank, df_embarc, df_bacteriams, df_spectrasweden)
 
#db <- getZiegler()

levels <- c("genus", "species")

genusPredModes <- c(2)
#genusPredModes <- c(0,1,2)

withPTMs <- c(2) 
#withPTMs <- c(0,1,2) 

combineExps <- c(FALSE) 
#combineExps <- c(FALSE,TRUE) 

#penalties <- c(1, 0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001)
penalties <- c(1000,100,10,2,1)

queryModifs <- c("none", "doubleAdded", "replacedByDouble")
#queryModifs <- c("none")

i <- 1

len <- 1000 
result_df <-data.frame(precision=character(), method = character(), level = character(), queryModif = character(), genusPredMode = numeric(), withPTM=numeric(), combineExp=character(), penalty=numeric(), acc=numeric(), kappa=numeric(), macroPrecision=numeric(), macroRecall=numeric(), macroF1=numeric(), wrongButCorrectAtGenusLevel=numeric(), accGenusLevel=numeric(), nc=numeric(), n_true_classes=numeric(), instances=numeric(), stringsAsFactors = FALSE)
 
for (precision in precisions){
  print(paste("precision: ",precision,  sep=""))
  for(level in levels){
    print(paste("level: ",level,  sep=""))
    for(withPTM in withPTMs){
      print(paste("withPTM: ",withPTM,  sep=""))
      for (queryModif in queryModifs){
        print(paste("queryModif: ",queryModif,  sep=""))
        for(combineExp in combineExps){
          print(paste("combineExp: ",combineExp,  sep=""))
          if (combineExp==TRUE){
            for(penalty in penalties){
              print(paste("penalty: ",penalty,  sep=""))
              if (level == "genus"){
                for(genusPredMode in genusPredModes){
                  print(paste("genusPredMode: ",genusPredMode,  sep=""))
                  combined <- combineConfs(i, confs, result_df, db,  sigma, level, precision, method, TRUE, genusPredMode, withPTM, penalty, combineExp, queryModif)
                  confs <- combined[[1]]
                  result_df <- combined[[2]]
                  i <- i + 1
                }
              } else {
                combined <- combineConfs(i, confs, result_df, db,  sigma, level, precision, method, TRUE, 0, withPTM, penalty, combineExp, queryModif)
                confs <- combined[[1]]
                result_df <- combined[[2]]
                i <- i + 1
              }
    
            }
          } else{
            if (level == "genus"){
              for(genusPredMode in genusPredModes){
                print(paste("genusPredMode: ",genusPredMode,  sep=""))
                combined <- combineConfs(i, confs, result_df, db,  sigma, level, precision, method, TRUE, genusPredMode, withPTM, NA, combineExp, queryModif)
                confs <- combined[[1]]
                result_df <- combined[[2]]
                i <- i + 1
              }
            } else {
              combined <- combineConfs(i, confs, result_df, db,  sigma, level, precision, method, TRUE, 0, withPTM, NA, combineExp, queryModif)
              confs <- combined[[1]]
              result_df <- combined[[2]]
              i <- i + 1
            }
          }
        }
      }
    }
    
  }
}

# Add ID so result can be matched to ID of confusion matrix
result_df$ID <- seq.int(nrow(result_df))
