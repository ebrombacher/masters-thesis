library(MALDIquant)
library(Cairo)
library(mlr)
library(caret)
source("functions.R")
######################################################################################################
# CREATE TRAINING AND TEST SET OF EXPERIMENTAL DATA

selected_rank  <- "genus"
factor <- FALSE
approach <- "peaks" # bins, fixpoint, peaks

# import Robert Koch csv
df <- getRobertKoch()
df_spectrabank <- getSpectraBank()
df_embarc <- getEmbarc()
df_bacteriams <- getBacteriaMS()
df_spectrasweden <- getSpectraSweden()

df2_all <- combineSourceDataframes(list(df, df_spectrabank, df_embarc, df_bacteriams, df_spectrasweden), approach, selected_rank, factor)

reduced <- reduceToEntriesWithAtLeast(df2_all, 4) #4

reduced$rank <- as.factor(reduced$rank)
badCols <- nearZeroVar(reduced)
reduced <-subset(reduced, select=-badCols)
# dput( as.numeric(sub('.', '', colnames(reduced[,rank:=NULL]))))
#write.csv(reduced, file="trainingdata_genus_atleast4_peaks.csv", row.names = FALSE)

set.seed(1)
sets <- getTrainTestSet(reduced)
train <- sets[[1]]
test <- sets[[2]]

######################################################################################################
# TRAIN CLASSIFIER
  
tasks <- getTasks(train, test)
trainTask <- tasks[[1]]
testTask <- tasks[[2]]

wts <- max(table(train$rank)) / table(train$rank)

#train a model
rf <- makeLearner("classif.LiblineaRL2LogReg", predict.type = "prob",  par.vals = list(cost=10))

rf$par.vals <- list(
  importance = TRUE
)

rforest <- mlr::train(rf, trainTask)

pred <- predict(rforest, testTask)
pred_df <- t(getPredictionProbabilities(pred))

conf <- calculateConfusionMatrix(pred)$result
perf <- performance(pred, measures = logloss)
print(perf)

