# FUNCTIONS:

# Get probability from normal distribution
probab <- function(p, std_dev, mean){
  prob <- (1/sqrt(2*pi*std_dev))*exp(-((p-mean)^2/(2*std_dev^2)))
  return(prob)
}

# Get a representative value for molecular weight calculated depending on how many different molecular weights are found for one protein of a defined bacterium
singlevalue <- function(mw_final_protonated, organism_code, species, protein){
  if (length(unique(mw_final_protonated)) == 1){
    return(unique(mw_final_protonated))
  }else if (length(mw_final_protonated) == 2){
    # Probability density function is used to choose the most likely weight. 
    mw_final_protonated <- unique(mw_final_protonated)
    selected_genus <- sub('(^\\w+)\\s.+','\\1',species[[1]])
    tmp <- genusmeans[genusmeans$genus==selected_genus&genusmeans$protein==protein[[1]],]
    p <- probab(mw_final_protonated, tmp$std, tmp$mean[[1]])
    
    return(mw_final_protonated[which.max(probab(mw_final_protonated, tmp$std, tmp$mean[[1]]))])
  } else {
    # Get the weighted mean of all weights
    tmp <- selected_weightedmean[selected_weightedmean$species==species[[1]]&selected_weightedmean$protein==protein[[1]],]
    return(mw_final_protonated[which.min(abs(mw_final_protonated - tmp$V1))])
  }
}

###################################################################################################################

# Get all files in folder KEGG_csv
file_list <- list.files("KEGG_csv", full.names = TRUE)
dataset <- data.frame(matrix(ncol = 11, nrow = 0))
x <- c("id","name","organism_code","organism_name","organism_phylogeny","aa_seq","protein","mw","met_cleavage","mw_final","mw_final_protonated")
colnames(dataset) <- x

for (file in file_list){
  # If the merged dataset doesn't exist, create it
  if (!exists("dataset")){
    dataset <- read.csv(file, header=TRUE)
  }
  
  # If the merged dataset does exist, append to it
  if (exists("dataset")){
    temp_dataset <-read.csv(file, header=TRUE)
    dataset<-rbind(dataset, temp_dataset)
    rm(temp_dataset)
  }
}

dataset$genus <- sub('(^\\w+)\\s.+','\\1',dataset$organism_name)
dataset$species <- sub('^(\\w+\\s+\\w+).*', '\\1', dataset$organism_name)

modified_proteins <- c("S1","S5", "S6", "S7", "S9", "S11", "S12", "S18", "L3", "L7/L12", "L11", "L14", "L16", "L33")
modified_weights <- c(2, 43, 129, 14, 42, 14, 46, 42, 14, 56, 126, 3, 14, 14)
modif <- setNames(as.list(modified_weights), modified_proteins)

# Add PTMs to protein weight
for (modified_protein in modified_proteins){
  dataset$mw_final_protonated[dataset$protein==modified_protein] <- dataset$mw_final_protonated[dataset$protein==modified_protein]+modif[[modified_protein]]
}

library(data.table)
selected <- dataset[,c("organism_code", "organism_name", "species", "genus", "protein","mw_final_protonated")]

any(is.na(selected))
#[1] FALSE ---> no NAs in selected

library(plyr)
selected_weightedmean <- ddply(selected, .(species,protein), function(x) weighted.mean(x[,"mw_final_protonated"])) 

library(dplyr)
genusmeans <- dataset %>% 
  group_by(genus, protein) %>% 
  summarise(mean=mean(mw_final_protonated), std=sd(mw_final_protonated))


# Aggregate dataframe to receive one specific weight for each protein of a bacterium
library(dplyr)
selected_aggr <- selected %>% 
  group_by(organism_code, organism_name, species, genus, protein) %>% 
  summarise_all(funs(singlevalue(mw_final_protonated, organism_code, species, protein)))

proteins <- sort(unique(dataset$protein))

library(reshape2)
cast <- dcast(selected_aggr, organism_code + organism_name + species ~ protein, value.var = "mw_final_protonated")

species <- cast$species
genus <- sub('(^\\w+)\\s.+','\\1',cast$species)
cast$species <- NULL

organism_code <- cast$organism_code
cast$organism_code <- NULL

strain <- cast$organism_name
cast$organism_name <- NULL

# Ignore weight values above 40000 Da
cast[cast>40000&!is.na(cast)]=NA

cast_genus <- cbind(cast, genus)
cast_species <- cbind(cast, species)
cast_strain <- cbind(cast, strain)


#write.csv(cast_strain, "kegg_combined_strain.csv", row.names = FALSE)
#write.csv(cast_species, "kegg_combined_species.csv", row.names = FALSE)
#write.csv(cast_genus, "kegg_combined_genus.csv", row.names = FALSE)

#cast_strain <- read.csv("kegg_combined_strain.csv")
#cast2 <- cast[cast$species!="Helicobacter pylori",]

# The followinf files are used for the creation of the Naive  Bayes model in Java
library(farff)
writeARFF(cast_species, "kegg_combined_species_arff.txt")
writeARFF(cast_genus, "kegg_combined_genus_arff.txt")

######################################################

# GET TOP 20 MOST ABUNDANT SPECIES AND GENERA

library(plyr)
# Frequencies
cast_strain <- read.csv("kegg_combined_strain.csv")
cast_species <- read.csv("kegg_combined_species.csv")
cast_genus <- read.csv("kegg_combined_genus.csv")

freq_species <- count(cast_species$species)
colnames(freq_species) <- c("bacterium", "frequency")
#write.csv(freq_species, "kegg_species_frequencies.csv", row.names = FALSE)
freq_species <- freq_species[order(-freq_species$frequency),]
freq_species_top20 <- freq_species[1:20,]
#write.csv(freq_species_top20, "kegg_species_frequencies_top20.csv", row.names = FALSE)

freq_genus <- count(cast_genus$genus)
colnames(freq_genus) <- c("bacterium", "frequency")
#write.csv(freq_genus, "kegg_genus_frequencies.csv", row.names = FALSE)
freq_genus <- freq_genus[order(-freq_genus$frequency),]
freq_genus_top20 <- freq_genus[1:20,]
#write.csv(freq_genus_top20, "kegg_genus_frequencies_top20.csv", row.names = FALSE)

