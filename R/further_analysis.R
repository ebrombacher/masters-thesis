source("functions.R")

# REMOVE ALL PTMs EXCEPT FOR METHIONINE CLEAVAGE

cast_strain <- read.csv("kegg_combined_strain.csv")

cast_strain$strain <- gsub("\\ ", "_", cast_strain$strain)
cast_strain$strain <- gsub("\\.", "_", cast_strain$strain)
cast_strain$strain <- gsub("__", "_", cast_strain$strain)

rownames(cast_strain) = make.names(cast_strain$strain, unique=TRUE)

# Removal of PTMs added previously
modified_proteins <- c("S1","S5", "S6", "S7", "S9", "S11", "S12", "S18", "L3", "L7.L12", "L11", "L14", "L16", "L33")
modified_weights <- c(2, 43, 129, 14, 42, 14, 46, 42, 14, 56, 126, 3, 14, 14)
modif <- setNames(as.list(modified_weights), modified_proteins)


require("dplyr")
for (modified_protein in modified_proteins){
  idx <- which(colnames(cast_strain)==modified_protein)
  cast_strain[,idx] <- cast_strain[,idx]-modif[[modified_protein]]
}

cast_strain <- cast_strain[ ,c("L1","L2","L3","L4","L5","L6","L7.L12","L7A","L9","L10","L11","L13","L14","L15","L16","L17","L18","L19","L20","L21","L22","L23","L24","L25","L27","L28","L29","L30","L31","L32","L33","L34","L35","L36","S1","S2","S3","S4","S5","S6","S7","S8","S9","S10","S11","S12","S13","S14","S15","S16","S17","S18","S19","S20","S21","strain")]

#write.csv(cast_strain, file="kegg_combined_strain_PTMsRemoved.csv", row.names = FALSE)
#########################################################################################################
# COMPARISON VIOLIN PLOT EXPERIMENTAL VS IN SILICO MASSES

df <- getRobertKoch()
df_spectrabank <- getSpectraBank()
df_embarc <- getEmbarc()
df_bacteriams <- getBacteriaMS()
df_spectrasweden <- getSpectraSweden()
df_combined <- qpcR:::cbind.na(df, df_spectrabank, df_embarc, df_bacteriams, df_spectrasweden)
df_combined_vec = as.numeric(unlist(df_combined))
df_combined_vec <- df_combined_vec[!is.na(df_combined_vec)]
mean(df_combined_vec)
# [1] 5861.537
median(df_combined_vec)
# [1] 5380.519
df_combined_vec = data.frame(Datasets = "Experimental", Mass = df_combined_vec)

cast_strain$strain <- NULL
cast_strain_vec = as.numeric(unlist(cast_strain))
cast_strain_vec <- cast_strain_vec[!is.na(cast_strain_vec)]
mean(cast_strain_vec)
#  14175.44
median(cast_strain_vec)
# 13352.93
cast_strain_vec = data.frame(Datasets = "In silico", Mass = cast_strain_vec)

plot.data <- rbind(df_combined_vec, cast_strain_vec)

library(ggpubr)

library(ggplot2)
p <- ggplot(plot.data, aes(x=Datasets, y=Mass, fill=Datasets)) +
  #geom_point(aes(color=Datasets), alpha=0.05, position='jitter') + 
  geom_violin(alpha=0.5) +
  geom_boxplot(width=0.1, alpha=0.2) +
  stat_summary(fun.y=mean, geom="point", color="black", fill="black") +
  scale_fill_grey()+
  theme(legend.position="none") +
  scale_y_continuous(breaks = seq(0, 42000, by=2500)) +
  ylab("Mass [Da]")


my_comparisons <- list( c("Experimental", "In silico"))
p + stat_compare_means(method = "t.test", comparisons = my_comparisons) # Add pairwise comparisons p-value


#########################################################################################################
# FACET WRAP DENSITY PLOTS FOR EACH RIBOSOMAL PROTEIN

library(plyr)
library(tidyr)
library(purrr)
library(Cairo)
Cairo(file="facetWrap_densityPlots_insilico_ribosomalProteins_PTMsRemoved.pdf", type="pdf", units="in", width=100, height=70, pointsize=12, dpi=300)
cast_strain %>%
  keep(is.numeric) %>%                     # Keep only numeric columns
  gather() %>%                             # Convert to key-value pairs
  ggplot(aes(value)) +                     # Plot the values
  facet_wrap(~ key, scales = "fixed") +   # In separate panels
  geom_density()                         # as density
dev.off()

## get medians of ribosomal proteins
medians <- data.frame(apply(data.matrix(cast_strain[, names(cast_strain) != "strain"]  ),2,median, na.rm=TRUE))
medians$protein <- row.names(medians)
names(medians)[1]<-"median"
row.names(medians) <- NULL
medians <- medians[order(medians$median),] 
medians <- medians[ , c("protein", "median")]
medians$median <- round(medians$median, digits=1)


protein_order <- c("L1","L2","L3","L4","L5","L6","L7.L12","L7A","L9","L10","L11","L13","L14","L15","L16","L17","L18","L19","L20","L21","L22","L23","L24","L25","L27","L28","L29","L30","L31","L32","L33","L34","L35","L36","S1","S2","S3","S4","S5","S6","S7","S8","S9","S10","S11","S12","S13","S14","S15","S16","S17","S18","S19","S20","S21")
# Make chrom column ordered. Second term defines the order
medians$protein<- ordered(medians$protein, protein_order)
medians <- medians[order(medians$protein),]

#write.csv(medians, file="median_insilico_ribosomalProteins_PTMsRemoved.csv", row.names = FALSE)
#########################################################################################################

# BIPLOT

library(plyr) 
library(dplyr)
library(Cairo)

library(pca3d)
library(factoextra)
source("ggbiplot2.R")

#pca=prcomp(subset(df2_all, select = -c(rank)),scale.=TRUE)
cast_strain$strain = sub("_.*", "", cast_strain$strain) 

top <- as.vector(names(sort(summary(as.factor(cast_strain$strain)), decreasing=T)[1:15]))
cast_strain_top <-cast_strain[cast_strain$strain %in% top,]
x <- scale(subset(cast_strain_top, select = -c(strain))) 
x[is.na(x)] <- 0 
x <- data.frame(x)
pca <- prcomp(x, scale=TRUE, center=TRUE) 

# pca=prcomp(subset(cast_strain, select = -c(strain)),scale.=TRUE)

CairoPDF(width = 25, height = 25,file="biplot_insilico_ribosomalProteins_top15_PTMsRemoved.pdf", bg="white")
ggbiplot2(pca,obs.scale = 1, var.scale=1,groups=cast_strain_top$strain, ellipse=TRUE,circle=TRUE, varname.size = 3, varname.adjust=3, alpha_arrow = 0.3, varname.alpha = 0.5)
dev.off() # creates a file with the above plot

sd <- pca$sdev
loadings <- pca$rotation
rownames(loadings) <- colnames(x)
scores <- pca$x

var <- sd^2
var.percent <- var/sum(var) * 100
CairoPDF(width = 25, height = 25,file="biplot_insilico_ribosomalProteins_top15_spreePlot_PTMsRemoved.pdf", bg="white")
barplot(var.percent, xlab='PC', ylab='Percent Variance', names.arg=1:length(var.percent), las=1, ylim=c(0, max(var.percent)), col='gray')
abline(h=1/ncol(cast_strain_top)*100, col='red')
dev.off()


#A second criteria we should consider is which principal components explain more than one variable’s worth of information. Since we have eight variables, if each variable contributed equally, they would each contribute 12.5% to the total variance, indicated by the red line. This criterion suggests we should also include principal component 3 (barely) in our interpretation. When we report our results, we should state the explained variance of each principal component we used, plus their combined explained variance, so we’ll calculate those exactly:
var.percent[1:14]
#[1] 15.687805  9.237123  7.803089  7.010034  5.146265  3.269359  2.931499  2.671975
#[9]  2.581985  2.456678  2.366263  2.163726  1.970124  1.874953
sum(var.percent[1:14])
#67.17088

loadings
sqrt(1/ncol(x))	# cutoff for 'important' loadings
#[1] 0.13484

loadings_sorted <- data.frame(loadings[,1:14])
loadings_sorted <- loadings_sorted[order(-abs(loadings_sorted$PC1),-abs(loadings_sorted$PC2),-abs(loadings_sorted$PC3), -abs(loadings_sorted$PC4),-abs(loadings_sorted$PC5)),]
#write.csv(loadings_sorted, file="biplot_insilico_ribosomalProteins_top15_first14components_PTMsRemoved.csv")

#########################################################################################################
#CORRPLOT

library(corrplot)
# generating large feature matrix (cols=features, rows=samples)
df2 <- subset(cast_strain, select = -c(strain))
df2[is.na(df2)] <- 0 

library(caret)
badCols <- nearZeroVar(df2)
badCols <- colnames(as.data.frame(df2)[, badCols])
df2 <-df2[ , !(names(df2) %in% badCols)]

correlationMatrix <- cor(df2)

num_features <- ncol(df2) # how many features
num_samples <- nrow(df2) # how many samples

library(Cairo)

cor.mtest <- function(mat, ...) {
  mat <- as.matrix(mat)
  n <- ncol(mat)
  p.mat<- matrix(NA, n, n)
  diag(p.mat) <- 0
  for (i in 1:(n - 1)) {
    for (j in (i + 1):n) {
      tmp <- cor.test(mat[, i], mat[, j], ...)
      p.mat[i, j] <- p.mat[j, i] <- tmp$p.value
    }
  }
  colnames(p.mat) <- rownames(p.mat) <- colnames(mat)
  p.mat
}
# matrix of the p-value of the correlation
p.mat <- cor.mtest(df2)


Cairo(file="corrplot_insilico_ribosomalProteins_withValues_PTMsRemoved", type="pdf", units="in", width=100, height=70, pointsize=30, dpi=300)

corrplot(correlationMatrix, method="color",
         type="upper", order="hclust", 
         addCoef.col = "black", # Add coefficient of correlation
         tl.col="black", tl.srt=45, #Text label color and rotation
         # Combine with significance
         p.mat = p.mat, insig = "blank", 
         number.cex = .3, tl.cex = 0.45,  
         # hide correlation coefficient on the principal diagonal
         diag=FALSE 
)

dev.off()


highlyCorrelated <- findCorrelation(correlationMatrix, cutoff=0.75)
# print indexes of highly correlated attributes
print(highlyCorrelated)

highlyCorrelatedNames <- colnames(correlationMatrix)[highlyCorrelated]
highlyCorrelatedNames

#[1] "L16" "L15" "L6"  "S4"  "L1"  "L3"  "S8"  "L5" 

#########################################################################################################
# BORUTA PLOT
## EXPERIMENTAL w/ and w/o PTMs

# w/ and w/o PTMs, genusMode2
generateMap <- function(query, sigma, level){

  path <- paste("naiveBayes_species_precision1.csv",  sep="")
  path2 <- paste("naiveBayes_species_precision1_PTMsRemoved.csv",  sep="")
  
  df_database <- rbind(read.csv(path,header=T), read.csv(path2,header=T))
  
  # Remove duplicate rows resulting from combining the data frames
  df_database <- unique(df_database)

  df_database$keep <- FALSE
  
  map <- data.frame(matrix(nrow = length(unique(df_database$class)), ncol = length(unique(df_database$protein))))
  colnames(map) <- unique(df_database$protein)
  rownames(map) <- unique(df_database$class)
  
  none <- 6.049268112978591*10^(-6)
  
  map[is.na(map)] <- none
  
  for (p in query){
    df_database$keep[(df_database$mean<=(p+sigma))&(df_database$mean>=(p-sigma))]=TRUE
  }
  
  df_database <- df_database[df_database$keep==TRUE,]
  
  # if any entry is left after pre-selection
  if (nrow(df_database)>0){
    masses  <- rev(expand.grid(list(bacterium=unique(df_database$class),proteins=unique(df_database$protein))))
    namevector <- c("mass", "query_mass", "probab")
    masses[ , namevector] <- NA
    
    if (nrow(df_database)>0){
      df_database$keep <- NULL
      
      for (p in query){
        # go through each row of db 
        for(i in 1:nrow(df_database)) { 
          
          #get species, protein, std_dev, mean
          class <- df_database[i,1]
          protein <- df_database[i,2]
          std_dev <- df_database[i,4]
          mean <- df_database[i,6]
          
          # calculate probability
          new <- probab(p, std_dev, mean)
          if(new>map[as.character(class), as.character(protein)]){
            map[as.character(class), as.character(protein)] <- new
            masses$mass[masses$bacterium==as.character(class)&masses$proteins==as.character(protein)] <- mean
            masses$query_mass[masses$bacterium==as.character(class)&masses$proteins==as.character(protein)] <- p
            masses$probab[masses$bacterium==as.character(class)&masses$proteins==as.character(protein)] <- new
            #print(as.character(class))
          }
        }
      }
    }  
    map <- transform(map, prod=Reduce(`*`, map))
    
    masses <- masses[!is.na(masses$mass),]
    
    # if multiple rows with same bacterium and query_mass keep row with highest value of probab
    masses <- masses[order(masses$bacterium, masses$query_mass, -abs(masses$probab) ), ] ### sort first
    masses <- masses[!duplicated(masses[,c("bacterium","query_mass")]),]
    masses$probab <- NULL
    
    masses <- masses[with(masses, order(bacterium, mass)),]
    masses <- data.frame(lapply(masses, as.character), stringsAsFactors=FALSE)
    
    masses <- aggregate(. ~ bacterium, data = masses, paste, collapse = ",")
    
    freq <- read.csv("kegg_species_frequencies.csv")
    freq$bacterium <- gsub("\\ ", "_", freq$bacterium)

    # total no. of samples
    total <- sum(freq$frequency)
    freq$proportion <- freq$frequency/total
    map$bacterium <- rownames(map)
    
    library(plyr)
    final_map <- join(map,freq,by='bacterium')
    final_map$result <- final_map$prod*final_map$proportion
    
    final_map <- join(final_map,masses,by='bacterium')
    final_map <- final_map[order(-final_map$result),] 
    
    return(final_map)
  } else {
    return(FALSE)
  }
}


# genus prediction mode: 0=use all bacteria of the respective genus, 1=use only bacteria if they belong to a species class in training dataset, 2=1 plus use model for species and extract genus afterwards
getConfMatrix <- function(test, sigma, level, matrix){

  freq <- read.csv("kegg_species_frequencies.csv")
  freq$bacterium <- gsub("\\ ", "_", freq$bacterium)
  colnames(test) <- gsub("^([^_]*_[^_]*)_.*$", "\\1", colnames(test))
  colnames(test) <- gsub("\\..*","", colnames(test))
  
  intersect <- intersect(unique(freq$bacterium), unique(colnames(test)))
  test <- test[ ,colnames(test) %in% intersect]

  names <- colnames(test)
  names <- gsub("\\..*","",names)
  numQueries <- ncol(test)

  true <- c()
  prediction <- c()
  
  for (i in 1:numQueries){
    trueClass <- names[i]
    
    if(level == "genus"){
      trueClass <- sub("_.*", "", trueClass)
    }
    print(trueClass)
    true <- c(true, trueClass)
    
    #print(query)
    query <- test[,i]
    query <- query[!is.na(query)]

    map <- generateMap(query, sigma, level) 
    if (length(map)>1){
      max <- map$bacterium[which.max(map$result)]
      if(level == "genus"){
        max <- sub("_.*", "", max)
      }
      #print(max)
    } else {
      max <- "none"
    }
    prediction <- c(prediction, max)
  }
  
  if (matrix == TRUE){
    library(caret)
    lvs <- union(prediction, true)
    prediction <- factor(prediction,levels = lvs)
    true <- factor(true,levels = lvs)
    conf <- caret::confusionMatrix(prediction,true, mode="everything")
    
    return(conf)
  } else {
    return(prediction)
  }
}

library(MALDIquant)
library(Cairo)
library(mlr)
library(caret)
source("functions.R")

selected_rank  <- "genus"
factor <- FALSE
approach <- "peaks" # bins, fixpoint, peaks

# import Robert Koch csv
df <- getRobertKoch()
df_spectrabank <- getSpectraBank()
df_embarc <- getEmbarc()
df_bacteriams <- getBacteriaMS()
df_spectrasweden <- getSpectraSweden()

queryFrame <- qpcR:::cbind.na(df, df_spectrabank, df_embarc, df_bacteriams, df_spectrasweden)

freq <- read.csv("kegg_species_frequencies.csv")
freq$bacterium <- gsub("\\ ", "_", freq$bacterium)

insilico <- getConfMatrix(queryFrame,  3, selected_rank, FALSE)

df2_all <- combineSourceDataframes(list(df, df_spectrabank, df_embarc, df_bacteriams, df_spectrasweden), approach, "species", factor)

intersect <- intersect(unique(freq$bacterium), unique(df2_all$rank))

df2_all <- df2_all[df2_all$rank %in% intersect,]
if (selected_rank == "genus"){
  df2_all$rank = factor(sub("_.*", "", df2_all$rank))
}

df2_all$insilico <- factor(insilico) # only if in silico feature is included

if (selected_rank=="species"){
  df2_all$insilico_genus = factor(sub("_.*", "", df2_all$insilico)) # for species only and if genus feature is added
}

reduced <- reduceToEntriesWithAtLeast(df2_all, 4) #4


library(Boruta)
library(Cairo)
library(caret)

badCols <- nearZeroVar(reduced)
reduced <-subset(reduced, select=-badCols)
seed<-1
set.seed(seed)
boruta.train <- Boruta(rank ~. , data = reduced, doTrace = 1)
print(boruta.train)

Cairo(file="boruta_insilico_genus_atleast4_wAndwoPTMS_peaksApproach.pdf", type="pdf", units="in", width=100, height=70, pointsize=9, dpi=300)
par(mar=c(11,3,1,1)) 
plot(boruta.train, xlab = "", xaxt = "n")
lz<-lapply(1:ncol(boruta.train$ImpHistory),function(i)
  boruta.train$ImpHistory[is.finite(boruta.train$ImpHistory[,i]),i])
names(lz) <- colnames(boruta.train$ImpHistory)
Labels <- sort(sapply(lz,median))
axis(side = 1,las=2,labels = names(Labels),
     at = 1:ncol(boruta.train$ImpHistory), cex.axis = 1.0)

dev.off()  


final.boruta <- TentativeRoughFix(boruta.train)
selected_features <- getSelectedAttributes(final.boruta, withTentative = F)
boruta.df <- attStats(final.boruta)
dput(selected_features)

#write.csv(boruta.df, file="boruta_insilico_genus_atleast4_wAndwoPTMS.csv")
write.csv(boruta.df, file="boruta_insilico_genus_atleast4_wAndwoPTMS_peaksApproach.csv")

#########

# BORUTA PLOT
# IN SILICO, w/o PTMs
cast_strain2 <- cast_strain

selected_rank  <- "species"

if (selected_rank=="genus"){
  cast_strain2$strain = sub("_.*", "", cast_strain2$strain)
} else if (selected_rank=="species"){
  cast_strain2$strain <-gsub("^([^_]*_[^_]*)_.*$", "\\1", cast_strain2$strain) 
}

cast_strain2[is.na(cast_strain2)] <- 0
#badCols <- nearZeroVar(cast_strain2)

# badCols
# [1] 34 55
# > colnames(cast_strain2)[34]
# [1] "S1"
# > colnames(cast_strain2)[55]
# [1] "L7A"

#cast_strain2 <-subset(cast_strain2, select=-badCols)

seed<-1
set.seed(seed)
cast_strain2$strain <- as.factor(cast_strain2$strain)
boruta.train <- Boruta(strain ~. , data = cast_strain2, doTrace = 1)
print(boruta.train)

Cairo(file="boruta_insilicoRibosomalProteinsOnly_species_wAndwoPTMS.pdf", type="pdf", units="in", width=100, height=70, pointsize=12, dpi=300)
par(mar=c(11,3,1,1)) 
plot(boruta.train, xlab = "", xaxt = "n")
lz<-lapply(1:ncol(boruta.train$ImpHistory),function(i)
  boruta.train$ImpHistory[is.finite(boruta.train$ImpHistory[,i]),i])
names(lz) <- colnames(boruta.train$ImpHistory)
Labels <- sort(sapply(lz,median))
axis(side = 1,las=2,labels = names(Labels),
     at = 1:ncol(boruta.train$ImpHistory), cex.axis = 1.7)

dev.off()  


final.boruta <- TentativeRoughFix(boruta.train)
selected_features <- getSelectedAttributes(final.boruta, withTentative = F)
boruta.df <- attStats(final.boruta)

#write.csv(boruta.df, file="boruta_insilicoRibosomalProteinsOnly_genus_PTMSremoved.csv")
#write.csv(boruta.df, file="boruta_insilicoRibosomalProteinsOnly_species_PTMSremoved.csv")

dput(selected_features)
#########################################################################################################

# mlr ML CLASSIFIERS

filename <- "species_wInSilicoFeaturePlusGenusFeature_allSets_10foldCV_peaksApproach"

reduced$rank <- as.factor(reduced$rank)

set.seed(1)
sets <- getTrainTestSet(reduced)
train <- sets[[1]]
test <- sets[[2]]

set.seed(1)
library(caret)
tasks <- getTasks(train, test)
trainTask <- tasks[[1]]
testTask <- tasks[[2]]

set.seed(1)
# ## Create a list of learners
lrns = list(
  #makeLearner("classif.LiblineaRL2LogReg", predict.type = "prob", id="LiblineaRL2LogReg"), # not for insilico
  makeLearner("classif.C50", predict.type = "prob", id="C50"),
  makeLearner("classif.h2o.deeplearning", predict.type = "prob", id="h2o.deeplearning"),
  makeLearner("classif.h2o.gbm", predict.type = "prob", id="h2o.gbm"), # +
  makeLearner("classif.h2o.randomForest", predict.type = "prob", id="h2o.randomForest"),
  makeLearner("classif.kknn", predict.type = "prob", id="kknn"),  
  #makeLearner("classif.randomForest", predict.type = "prob", id="randomForest"), # not for species
  makeLearner("classif.randomForestSRC", predict.type = "prob", id="randomForestSRC"),
  makeLearner("classif.ranger", predict.type = "prob", id="ranger"),
  #makeLearner("classif.rda", predict.type = "prob", id="rda"), # +, but with NA, not for species
  makeLearner("classif.svm", predict.type = "prob", id="svm"),
  makeLearner("classif.featureless", predict.type = "prob",  id="featureless")
)

#set.seed(123, "L'Ecuyer")
set.seed(4)
rdesc = makeResampleDesc("CV", iters = 10)
meas = list(logloss, macrof1, mmce, acc, timetrain)

bmr = benchmark(lrns, trainTask, rdesc, meas, show.info = TRUE, keep.pred = TRUE)
bmr

perf = getBMRPerformances(bmr, as.df = TRUE)
perf

plt <- plotBMRBoxplots(bmr, measure = logloss, pretty.names = FALSE, style = "violin")

# getBMRModels(bmr)

levels(plt$data$task.id) = c("Species (with >3 entries), Peaks approach w/ two insilico features")

library(Cairo)
Cairo(file=paste(filename,".pdf",  sep=""), type="pdf", units="in", width=100, height=70, dpi=700)
plt 
dev.off()  

df = reshape2::melt(perf, id.vars = c("task.id", "learner.id", "iter"))
df = df[df$variable == "logloss",]
df = reshape2::dcast(df, task.id + iter ~ variable + learner.id)
head(df)


library(GGally)
g <- GGally::ggpairs(df, 3:ncol(df), upper = list(continuous = wrap("cor", size = 6)),
                     lower = list(continuous = "smooth")) + theme(
                       strip.text = element_text(size = 7),
                       axis.text.x = element_text(size=9),
                       axis.text.y = element_text(size=12))

Cairo(file=paste(filename,"_modelCorr.pdf",  sep=""), type="pdf", units="in", width=100, height=70, dpi=400)
g 
dev.off()  


##########################
# HEATMAP

cast_strain <- read.csv("kegg_combined_strain.csv")

cast_strain$strain <- gsub("\\ ", "_", cast_strain$strain)
cast_strain$strain <- gsub("\\.", "_", cast_strain$strain)
cast_strain$strain <- gsub("__", "_", cast_strain$strain)

rownames(cast_strain) = make.names(cast_strain$strain, unique=TRUE)

# Removal of PTMs added previously
modified_proteins <- c("S1","S5", "S6", "S7", "S9", "S11", "S12", "S18", "L3", "L7.L12", "L11", "L14", "L16", "L33")
modified_weights <- c(2, 43, 129, 14, 42, 14, 46, 42, 14, 56, 126, 3, 14, 14)
modif <- setNames(as.list(modified_weights), modified_proteins)

cast_strain_PTMsRemoved <- cast_strain
require("dplyr")
for (modified_protein in modified_proteins){
  idx <- which(colnames(cast_strain_PTMsRemoved)==modified_protein)
  cast_strain_PTMsRemoved[,idx] <- cast_strain_PTMsRemoved[,idx]-modif[[modified_protein]]
}
cast_strain$strain <- NULL
cast_strain <- cbind(cast_strain,cast_strain_PTMsRemoved)

#cast_strain <- cast_strain[ ,c("L1","L2","L3","L4","L5","L6","L7.L12","L7A","L9","L10","L11","L13","L14","L15","L16","L17","L18","L19","L20","L21","L22","L23","L24","L25","L27","L28","L29","L30","L31","L32","L33","L34","L35","L36","S1","S2","S3","S4","S5","S6","S7","S8","S9","S10","S11","S12","S13","S14","S15","S16","S17","S18","S19","S20","S21","strain")]


cast_strain$strain <- gsub("\\ ", "_", cast_strain$strain)
cast_strain$strain <- gsub("\\.", "_", cast_strain$strain)
cast_strain$strain <- gsub("__", "_", cast_strain$strain)

rownames(cast_strain) = make.names(cast_strain$strain, unique=TRUE)


cast_strain$strain <- NULL
#cast_strain$strain <- rownames(cast_strain) 

t_cast_strain <- as.data.frame(t(cast_strain))
rownames(t_cast_strain) <- NULL


# Generate dataframe containing feature values of the peaks approach
getDataFrame_peaks_strains <- function(df, query=NULL){
  tol <- 10
  if (!is.null(query)){
    library(qpcR)
    max <- max(query)
    df <- qpcR:::cbind.na(query, df)
    colnames(df)[1]  <- "query"
  } else {
    max <- 40000
  }
  
  library(MALDIquant)
  # bin peak lists
  peakList = list()
  for(i in names(df)){
    inner_list= df[i][!is.na(df[i])]
    inner_list <- inner_list[ inner_list <= max+tol ]
    
    if (i %in% names(peakList)){
      i <- paste(i, "_", ceiling(runif(1, 0, 10^12)), sep = "")
    }
    peakList[[i]] <- createMassPeaks(mass=unlist(inner_list), intensity=as.integer(rep(1,length(unlist(inner_list)))))
  }
  
  binnedPeaks <- binPeaks(peakList, method = "relaxed", tolerance =0.005) 
  
  ## generate binary matrix
  binned <- intensityMatrix(binnedPeaks)
  
  rownames(binned) <- names(peakList)
  
  df2 <- as.data.frame(binned)
  df2[is.na(df2)] <- 0
  return(df2)
}

intensMatrix_strains <- getDataFrame_peaks_strains(t_cast_strain)
colnames(intensMatrix_strains) <- round(as.numeric(colnames(intensMatrix_strains)))

level <- "genus"
if (level=="genus"){
  intensMatrix_strains$rank <- sub("_.*", "", rownames(intensMatrix_strains))
} else if (level=="species"){
  intensMatrix_strains$rank <- gsub("^([^_]*_[^_]*)_.*$", "\\1", rownames(intensMatrix_strains))
}
intensMatrix_strains$rank <- gsub("\\..*","", intensMatrix_strains$rank)

df2_all <- intensMatrix_strains

# sum all rows of one group
newdataframe <- aggregate(. ~ rank, df2_all, sum)
blub <- data.frame(table(df2_all$rank))
colnames(blub) <- c("rank", "Freq")
merged <- merge(blub, newdataframe, by="rank", all = T)

#devide all columns by size of group
merged[-(1:2)] <- merged[-(1:2)]/merged[,2][row(merged[-(1:2)])]

merged$Freq <- NULL

library(caret)
badCols <- nearZeroVar(merged)
merged <-subset(merged, select=-badCols)

final <-merged
row.names(final)  <- gsub("_", "\\ ", final$rank)
rank2 = sub("_.*", "",row.names(final)) 

write.csv(final, file="heatmaply_insilico_merged_genus.csv", row.names = FALSE)

library(heatmaply)
#for genera:
if (level=="genus"){
  heatmaply(final[-(1)], height=800, width=1000, cexCol = 0.25, cexRow = 0.35, Rowv=NULL, Colv=NULL,margins=c(50, 60, 5, 5),  file = paste("heatmaply_insilico_peaksApproach_",level,".pdf",  sep="") )
  heatmaply(final[-(1)], branches_lwd = 0.25, height=1200, width=1200, cexCol = 0.25, cexRow = 0.4,  k_col = 10, k_row = 10,, margins=c(50, 60, 5, 5),  file = paste("heatmaply_insilico_peaksApproach_dendrogram_wColorbar_",level,".pdf",  sep=""))
  #for species:
} else if (level=="species"){
  heatmaply(final[-(1)], height=1400, width=1100, cexCol = 0.35, cexRow = 0.1, Rowv=NULL, Colv=NULL, margins=c(50, 60, 5, 5),  file = paste("heatmaply_insilico_peaksApproach_",level,".pdf",  sep=""))
  heatmaply(final[-(1)], row_side_colors= data.frame(" " = rank2, check.names=F), , branches_lwd = 0.12, height=1200, width=1100, cexCol = 0.2, cexRow = 0.1,  k_col = 10, k_row = 10,, margins=c(50, 60, 5, 5),  file = paste("heatmaply_insilico_peaksApproach_dendrogram_wColorbar_",level,".pdf",  sep=""))
}