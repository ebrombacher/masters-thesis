Steps to run web interface:
- Download "Apache Tomcat" from tomcat.apache.org (Binary Distributions --> Core --> Mac: "zip", Windows: Download and run "32-bit/64-bit Windows Service Installer")
- Transfer webInterface.war to webapps folder of Tomcat installation folder
- Start Tomcat server (Mac: Type "Tomcat/bin/startup.sh" in command line; Windows: press "Stop" and then "Start" in the Tomcat app)
- Open http://localhost:8080/webInterface/ in browser
- Enter masses of query separated by ";" and press "Enter" (only once!). It might take a few seconds until results appear.

To run webInterface in Eclipse specify runtime environment of server first via Preferences --> Server --> "Runtime Environments" and set path to where Apache Tomcat server is installed on computer (for Mac e.g. in usr/local/apache-tomcat-9...)
Furthermore, it might be necessary to delete existing server in "Server" tab and create a new one.

