package naiveBayes.naiveBayes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
//import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.evaluation.NominalPrediction;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.rules.PART;
import weka.classifiers.trees.DecisionStump;
import weka.classifiers.trees.J48;
import weka.core.FastVector;
import weka.core.Instances;
import weka.core.SerializationHelper;
 
public class RunClassifier {
	public static BufferedReader readDataFile(String filename) {
		BufferedReader inputReader = null;
 
		try {
			inputReader = new BufferedReader(new FileReader(filename));
			
		} catch (FileNotFoundException ex) {
			System.err.println("File not found: " + filename);
		}
 
		return inputReader;
	}
 
	public static Evaluation classify(Classifier model,
			Instances trainingSet, Instances testingSet) throws Exception {
		Evaluation evaluation = new Evaluation(trainingSet);
 
		model.buildClassifier(trainingSet);
		evaluation.evaluateModel(model, testingSet);
 
		return evaluation;
	}
 
	public static double calculateAccuracy(FastVector predictions) {
		double correct = 0;
 
		for (int i = 0; i < predictions.size(); i++) {
			NominalPrediction np = (NominalPrediction) predictions.elementAt(i);
			if (np.predicted() == np.actual()) {
				correct++;
			}
		}
 
		return 100 * correct / predictions.size();
	}
 
	public static Instances[][] crossValidationSplit(Instances data, int numberOfFolds) {
		Instances[][] split = new Instances[2][numberOfFolds];
 
		for (int i = 0; i < numberOfFolds; i++) {
			split[0][i] = data.trainCV(numberOfFolds, i);
			split[1][i] = data.testCV(numberOfFolds, i);
		}
 
		return split;
	}
 
	public static void main(String[] args) throws Exception {
		// BufferedReader datafile = readDataFile("kegg_combined_species_arff.txt");
		BufferedReader datafile = readDataFile("kegg_combined_genus_arff.txt");

		NaiveBayes classifier = new NaiveBayes();
		classifier.setUseKernelEstimator(true);
		FastVector predictions = new FastVector();
		
		Instances train = new Instances(datafile);
        int lastIndex = train.numAttributes() - 1;
        
        train.setClassIndex(lastIndex);
        classifier.buildClassifier(train);
        System.out.println(classifier);
		
        
        // The text file received through the following line can be processed via command line by the following command
        // sed -i '' 's/ \{2,\}/\|/g' naivebayes_model_final.txt
        // Change file extension from .txt to .csv
        // Afterwards "|" can be used as delimiter
        // However: caution needs to be payed that for species "Clostridium saccharolyticum Clostridium saccharoperbutylacetonicum" 
        // and for genus "Enterobacter Enterobacteriaceae" will not be separated as only one space is in between --> needs to be resolved by hand
        Files.write(Paths.get("naiveBayes_genus_precision1_javaOutput.txt"), classifier.toString().getBytes(), StandardOpenOption.CREATE);
        
        
	}
}