# PEAKS APPROACH

getSampleDF_peaks <- function(sample){
  df<- read.csv(file=sample, header=TRUE, sep=",")
  names(df) <- gsub('\\.', '_', names(df))
  return(df)
}

getRank <- function(df, level){
  # remove double underscores
  rownames(df) <-  gsub("_+", "_",rownames(df))
  rank <- ""
  if (level=="genus"){
    #for genus
    rank = sub("_.*", "", rownames(df)) 
  }
  else if (level=="species"){
    # for species
    rank = gsub("^([^_]*_[^_]*)_.*$", "\\1", rownames(df)) 
    print(rank)
  }
  return(rank)
}



# MALDIquant
match.closest <- function (x, table, tolerance = Inf, nomatch = NA_integer_) {
  lIdx <- findInterval(x, table, rightmost.closed = FALSE, 
                       all.inside = TRUE)
  rIdx <- lIdx + 1L
  lIdx[lIdx == 0L] <- 1L
  lDiff <- abs(table[lIdx] - x)
  rDiff <- abs(table[rIdx] - x)
  d <- which(lDiff >= rDiff)
  lIdx[d] <- rIdx[d]
  if (any(is.finite(tolerance))) {
    if (any(tolerance < 0L)) {
      warning(sQuote("tolerance"), " < 0 is meaningless. Set to zero.")
      tolerance[tolerance < 0L] <- 0L
    }
    if (length(nomatch) != 1L) {
      stop("Length of ", sQuote("nomatch"), " has to be one.")
    }
    tolerance <- rep_len(tolerance, length(table))
    lDiff[d] <- rDiff[d]
    lIdx[lDiff > tolerance[lIdx]] <- nomatch
  }
  lIdx
}

# Generate dataframe of sample containing feature values of the peaks approach
getSampleDf_new <- function(df3, rank){
  cols_peaks_genus <- c(2530.4, 2561.1, 2590.6, 2949.4, 2994.7, 3024.8, 3045, 3066, 
                        3093.8, 3117.9, 3136.4, 3155.6, 3187.5, 3212.4, 3248.6, 3275.9, 
                        3308.3, 3324.3, 3343.4, 3400, 3421.4, 3444, 3469.5, 3497.2, 3519.6, 
                        3587.2, 3606, 3621.1, 3650.7, 3686.2, 3729.7, 3759.2, 3785.4, 
                        3815.8, 3854.8, 3886.8, 3906.7, 3955, 3989.8, 4024.9, 4064, 4085.2, 
                        4111.6, 4131, 4154.6, 4183.1, 4254.8, 4277.6, 4307.2, 4334.9, 
                        4349.4, 4371.8, 4409.5, 4433.8, 4454.8, 4481.3, 4503, 4535, 4559.8, 
                        4606.6, 4639.3, 4672.8, 4704.3, 4740.2, 4770.3, 4813.3, 4857.1, 
                        4889.6, 4941.6, 4986.8, 5027.2, 5061.2, 5083.1, 5105.1, 5126.4, 
                        5160.7, 5201.7, 5254.2, 5303.2, 5329.7, 5364.2, 5423.2, 5459.1, 
                        5494.3, 5528.4, 5555.2, 5603.9, 5677.9, 5729.7, 5800.9, 5884.9, 
                        5919.3, 6000.6, 6047.3, 6097.4, 6157.3, 6234.1, 6266.8, 6312.2, 
                        6351.2, 6388.5, 6423.4, 6463.8, 6495.2, 6528.6, 6555.3, 6600.3, 
                        6640.1, 6673.9, 6701.8, 6742.9, 6819.6, 6888.8, 6946.2, 7039.5, 
                        7108.2, 7165.6, 7213.9, 7268, 7308, 7369.6, 7421.6, 7461.2, 7504.5, 
                        7544.5, 7599.8, 7633.1, 7670.7, 7726.2, 7775.9, 7839.9, 7910.4, 
                        7981.1, 8079.1, 8137.2, 8256.4, 8321.7, 8359.9, 8419.7, 8617.8, 
                        8691.2, 8870.4, 8925.4, 8985, 9066.8, 9123.8, 9197.3, 9259.7, 
                        9331.2, 9396.7, 9451.8, 9506.5, 9547.5, 9584.4, 9625, 9713.1, 
                        9790.9, 9838.4, 9899.5, 9973.9, 10070.5, 10216.5, 10274.1, 10313.5, 
                        10363.8, 10482.7, 11217.7)
  cols_peaks_species <-c(2590.6, 2949.4, 3024.8, 3045, 3066, 3117.9, 3136.4, 3155.6, 
                         3187.5, 3212.4, 3248.6, 3275.9, 3308.3, 3324.3, 3343.4, 3400, 
                         3421.4, 3444, 3469.5, 3497.2, 3519.6, 3587.2, 3621.1, 3650.7, 
                         3686.2, 3729.7, 3759.2, 3785.4, 3815.8, 3854.8, 3886.8, 3906.7, 
                         3955, 3989.8, 4024.9, 4064, 4111.6, 4131, 4154.6, 4183.1, 4277.6, 
                         4307.2, 4334.9, 4349.4, 4371.8, 4409.5, 4433.8, 4454.8, 4481.3, 
                         4503, 4535, 4559.8, 4606.6, 4639.3, 4672.8, 4704.3, 4740.2, 4770.3, 
                         4813.3, 4857.1, 4889.6, 4941.6, 4986.8, 5027.2, 5061.2, 5105.1, 
                         5126.4, 5160.7, 5201.7, 5254.2, 5303.2, 5364.2, 5423.2, 5459.1, 
                         5494.3, 5528.4, 5555.2, 5603.9, 5677.9, 5729.7, 5800.9, 5884.9, 
                         5919.3, 6000.6, 6047.3, 6097.4, 6157.3, 6234.1, 6266.8, 6312.2, 
                         6351.2, 6388.5, 6423.4, 6495.2, 6528.6, 6555.3, 6600.3, 6640.1, 
                         6673.9, 6701.8, 6742.9, 6819.6, 6888.8, 6946.2, 7039.5, 7108.2, 
                         7165.6, 7213.9, 7268, 7308, 7369.6, 7421.6, 7461.2, 7504.5, 7544.5, 
                         7599.8, 7633.1, 7670.7, 7726.2, 7775.9, 7839.9, 7910.4, 7981.1, 
                         8079.1, 8137.2, 8256.4, 8321.7, 8359.9, 8419.7, 8691.2, 8870.4, 
                         8925.4, 8985, 9066.8, 9123.8, 9197.3, 9259.7, 9331.2, 9396.7, 
                         9451.8, 9506.5, 9547.5, 9584.4, 9625, 9713.1, 9790.9, 9838.4, 
                         9899.5, 9973.9, 10070.5, 10216.5, 10274.1, 10313.5, 10363.8, 
                         10482.7, 11217.7)
  
  # remove leading X
  #names(df3) <- sub('.', '', names(df3))
  if (rank=="genus"){
    classifier_cols_wo_X <- cols_peaks_genus
  } else if (rank=="species"){
    classifier_cols_wo_X <- cols_peaks_species
  }
  #print(classifier_cols_wo_X)
  closest <- match.closest(classifier_cols_wo_X ,sort(df3[,1]), tolerance=10)
  classifier_closest_indices <- which(!is.na(closest))
  classifier_closest_masses <- classifier_cols_wo_X[classifier_closest_indices]
  
  sample_closest_indices <- closest[!sapply(closest,is.na)]
  sample_closest_masses <- df3[sample_closest_indices,]
  #print(sample_closest_masses)
  matched_cols <- df3[sample_closest_indices,]
  names(matched_cols) <- classifier_cols_wo_X[classifier_closest_indices]
  sample_df <-setNames(data.frame(matrix(ncol = length(classifier_cols_wo_X), nrow = 0)), classifier_cols_wo_X)
  
  #library(dplyr)
  #result_df <- bind_rows(sample_df, matched_cols)
  
  binary_vector <- as.integer(grepl(paste0(as.vector(as.numeric(colnames(t(data.frame(matched_cols))))), collapse = "|"), as.vector(as.numeric(colnames(sample_df)))))
  result_df <- rbind(sample_df, binary_vector)
  colnames(result_df) <- colnames(sample_df)
  
  colnames(result_df) <- paste("X", colnames(result_df), sep = "")
  #result_df[is.na(result_df)] <- 0
  #result_df <-result_df[ df2_all$rank %in% rank_robertkoch, ]
  rank <- "test"
  result_df <- cbind(result_df, rank)
  return(result_df)
}


##############################################
# BINS APPROACH

findit <- function(x,vec){ 
  vec[which.min(abs(vec-x))] - x
} 

get_bin <- function(n, granularity) {
  rounded_to <- floor((n + (granularity / 2)) / granularity) * granularity
  rounded_to <- rounded_to / granularity
  return(rounded_to + 1)
}

make_features <- function(masses, biggest_expected_mass, granularity) {
  features <- matrix(0, nrow = dim(masses)[1], ncol = 1 + ceiling(biggest_expected_mass / granularity))
  #print(dim(features))
  for(row in 1:dim(masses)[1]) {
    for(col in 1:dim(masses)[2]) {
       if (!is.na(masses[row, col]) & masses[row, col] <= biggest_expected_mass){
	      #features[row, get_bin(masses[row, col], granularity)] =  features[row, get_bin(masses[row, col], granularity)] + 1 
	      features[row, get_bin(masses[row, col], granularity)] =  1
	   	}
    }
  }
  return(features)
}


getDataFrame_bins <- function(df, level, factor){
  
  A <- t(round(df, -2))
  max <- 20000
  g <- 80
  
  labels <- c(seq(from = 0, to = max, by = g))
  X <- as.data.frame(make_features(A, max, g))
  
  colnames(X) <- labels
  rownames(X) <- colnames(df)
  
  rank <- getRank(X, level)
  rank_robertkoch <- rank
  
  colnames(X) <- paste("X", colnames(X), sep = "")

  
  if(factor) {
    library(dplyr)
    X <- X %>%mutate_if(is.numeric,as.factor)

    lvls <- c("0","1")
    X[] <-  lapply(X, factor, levels=lvls)
  }
  df2_all <- cbind(X, rank)
  df2_all$XNA <- NULL
  return(df2_all)
}

badCols <- c("X0", "X80", "X160", "X240", "X320", "X400", "X480", "X560", 
"X640", "X720", "X800", "X880", "X960", "X1040", "X1120", "X1200", 
"X1280", "X1360", "X1440", "X1520", "X1600", "X1680", "X1760", 
"X1840", "X1920", "X2000", "X2160", "X2560", "X2960", "X3360", 
"X3760", "X4160", "X4560", "X4960", "X5360", "X5760", "X6160", 
"X6560", "X6960", "X7360", "X7760", "X8160", "X8560", "X8960", 
"X9360", "X9760", "X10160", "X10560", "X10640", "X10720", "X10800", 
"X10880", "X10960", "X11040", "X11120", "X11280", "X11360", "X11440", 
"X11520", "X11600", "X11680", "X11760", "X11840", "X11920", "X12000", 
"X12080", "X12160", "X12240", "X12320", "X12400", "X12480", "X12560", 
"X12640", "X12720", "X12800", "X12880", "X12960", "X13040", "X13120", 
"X13200", "X13280", "X13360", "X13440", "X13520", "X13600", "X13680", 
"X13760", "X13840", "X13920", "X14000", "X14080", "X14160", "X14240", 
"X14320", "X14400", "X14480", "X14560", "X14640", "X14720", "X14800", 
"X14880", "X14960", "X15040", "X15120", "X15200", "X15280", "X15360", 
"X15440", "X15520", "X15600", "X15680", "X15760", "X15840", "X15920", 
"X16000", "X16080", "X16160", "X16240", "X16320", "X16400", "X16480", 
"X16560", "X16640", "X16720", "X16800", "X16880", "X16960", "X17040", 
"X17120", "X17200", "X17280", "X17360", "X17440", "X17520", "X17600", 
"X17680", "X17760", "X17840", "X17920", "X18000", "X18080", "X18160", 
"X18240", "X18320", "X18400", "X18480", "X18560", "X18640", "X18720", 
"X18800", "X18880", "X18960", "X19040", "X19120", "X19200", "X19280", 
"X19360", "X19440", "X19520", "X19600", "X19680", "X19760", "X19840", 
"X19920", "X20000")



library(stats)


factor <- FALSE
df2 <- data.frame(sample)
rownames(df2) <- NULL
#df2 <- getTestSample(sample)
print(df2)

if (approach == "fixpoint"){
  df2_all2 <- getDataFrame_fixpoint(df2, selected_rank)
  df2_all2 <- df2_all2[ , !(names(df2_all2) %in% badCols)]
} else if (approach == "bins"){
  df2_all2 <- getDataFrame_bins(df2, selected_rank, factor)
  df2_all2 <- df2_all2[ , !(names(df2_all2) %in% badCols)]
} else if (approach == "peaks"){
  df2_all2 <- getSampleDf_new(df2, selected_rank)
  print(df2_all2)
}



features <- colnames(df2_all2)
features <- gsub("\\,", ".", features)
values <- as.numeric(df2_all2[1,])


#########################################


