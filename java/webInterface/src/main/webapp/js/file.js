
var webSocket = new WebSocket("ws://localhost:8080/webInterface/myechoendpoint");

//var msg = document.getElementById("txtMessage");
var msg = document.getElementById("masses");
var sigma = document.getElementById("sigma");

var genus_button = document.getElementById("genus");
var species_button = document.getElementById("species");


webSocket.onopen = function(msgEvent){
//	txtAreaEcho.value += "Connected ... \n";
};

webSocket.onmessage = function(msgEvent){
	
	// alert(JSON.parse(msgEvent.data)[30]["proteinInfo"][0]["protein"]);

	var values=msgEvent.data.split('*');

	var approachId;
	for (approachId = 0; approachId < values.length; approachId++) { 
		
		bacterium = values[approachId];
		
		var tableId;
		if (approachId == 0){
			tableId = "jsonTable_ml";
			var oTblReport = $("#"+tableId);
			 
		    oTblReport.DataTable ({
		        data :JSON.parse(bacterium),
		        columns: [
		            { data: 'name', 
		              title: "Name"},
		            { data: 'score',
		              title: "Probability"}
		        ],
		        "order": [[ 1, "desc" ]]
		    });
		} else if (approachId == 1 || approachId == 2){
			if (approachId == 1){
				tableId = "jsonTable_insilico";
			} else if (approachId == 2){
				tableId = "jsonTable_augmented";
			}
			
			var columns = addAllColumnHeaders(JSON.parse(bacterium), tableId);
			
			for (var i = 0; i < JSON.parse(bacterium).length; i++) {
				var row$ = $('<tr/>');
				for (var colIndex = 0; colIndex < columns.length; colIndex++) {
					var cellValue = JSON.parse(bacterium)[i][columns[colIndex]];
		
					if (cellValue == null) { cellValue = ""; }
					
					if (colIndex == 0){
						cellValue = cellValue.replace("_", " ");
					}
					
					var hits = cellValue.length;
					if (colIndex == 2 && cellValue[0]){
						finalInfo = "";
						for (var infoIndex = 0; infoIndex < hits; infoIndex++) {
							finalInfo += cellValue[infoIndex]["protein"]+" ("+cellValue[infoIndex]["queryMass"]+" --> "+cellValue[infoIndex]["mass"]+")";
							if (infoIndex != hits-1){
								finalInfo += ", ";
							}
						}
						
						cellValue = finalInfo;
					}
					row$.append($('<td/>').html(cellValue));
				}
				$("#"+tableId).append(row$);
			}
		
			separateTableHeadAndBody(tableId);
			
			$('#'+tableId).DataTable( {
			    destroy: true,
			    columns: [
		            { data: 'name', 
		              title: "Name"},
		            { data: 'score',
		              title: "-Log(Probab)"},
		            { data: 'proteinInfo',
			          title: "Matched proteins"}
		        ],
			    "order": [[ 1, "asc" ]]
			} );

		}
	}	
};

webSocket.onclose = function(msgEvent){
	//txtAreaEcho.value += "Disconnect ... \n";
};

webSocket.onerror = function(msgEvent){ 
	//txtAreaEcho.value += "Error ... \n";
};


/**
 *  Send Message
 */
function doSendMessage()
{
	var level;
	if (genus_button.checked){
		level = "genus";
	} else if (species_button.checked){
		level = "species";
	}
	webSocket.send(msg.value+"--"+level+"--"+sigma.value);
}

/**
 *  Close Connection
 */
function doCloseConnection(){
	webSocket.close();
}


function addAllColumnHeaders(myList, tableId) {
	var columnSet = [];
	var headerTr$ = $('<tr/>');

	for (var i = 0; i < myList.length; i++) {
		var rowHash = myList[i];
		for (var key in rowHash) {
			if ($.inArray(key, columnSet) == -1) {
				columnSet.push(key);
				headerTr$.append($('<th/>').html(key));
			}
		}
	}
	$("#"+tableId).append(headerTr$);
	return columnSet;
}

function separateTableHeadAndBody(tableId){
	var myTable = jQuery("#"+tableId);
	var thead = myTable.find("thead");
	var thRows =  myTable.find("tr:has(th)");

	if (thead.length==0){  //if there is no thead element, add one.
	    thead = jQuery("<thead></thead>").prependTo(myTable);    
	}

	var copy = thRows.clone(true).appendTo("#"+tableId+" thead");
	thRows.remove();
}


document.getElementById('masses').addEventListener('keypress', function(event) {
    if (event.keyCode == 13) {
    	doSendMessage();
		$('#jsonTable_insilico').DataTable().clear().destroy();
		$('#jsonTable_insilico').empty();
		$('#jsonTable_ml').DataTable().clear().destroy();
		$('#jsonTable_ml').empty();
		$('#jsonTable_augmented').DataTable().clear().destroy();
		$('#jsonTable_augmented').empty();
    }
});



$('#jsonTable_insilico').on( 'length.dt', function ( e, settings, len) {
	$(".lab").css("height", len*45 + "px");  
} );

$('#jsonTable_ml').on( 'length.dt', function ( e, settings, len) {
	$(".lab").css("height", len*45 + "px");  
} );

