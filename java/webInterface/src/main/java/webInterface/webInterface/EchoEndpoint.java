package webInterface.webInterface;

import java.io.IOException;
import java.util.List;

import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.server.ServerEndpoint;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.websocket.Session;

/**
 * 
 * @author TechBySample.com
 *
 */

@ServerEndpoint("/myechoendpoint")
public class EchoEndpoint {
	
	/**
	 * 
	 *  Method is called when a connection is established.
	 *  
	 * @param session
	 */
	 @OnOpen
	    public void onOpen(Session session){
	        System.out.println(session.getId() + " has opened a connection"); 
	        try {
	            session.getBasicRemote().sendText("Connection Established");
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        }
	    }
	 
	   /**
	     * Method is called when user closes the connection.
	     * 
	     * Note: You cannot send messages to the client from this method
	     */
	    @OnClose
	    public void onClose(Session session){
	        System.out.println("Session " +session.getId()+" has ended");
	    }
	
	/**
     * Method is called when a user sends a message to this server endpoint.
     * Method intercepts the message and allows us to react accordingly.
     */
    @OnMessage
    public void onMessage(String message, Session session){
        System.out.println("Message from " + session.getId() + ": " + message);
        try {
        	String[] parts = message.split("--");
        	String masses = parts[0]; 
        	String level = parts[1]; 
        	Integer sigma = Integer.parseInt(parts[2]); 
        	System.out.println(sigma);
        	
        	ExperimentalProcessing experimental = new ExperimentalProcessing();
        	experimental.setMasses(masses);
        	experimental.processQuery(experimental.getMasses());

            
            List<ExperimentalBacterium> ml_result = experimental.getBacteriaList(level, experimental.getQuery());    
        	
        	
            InsilicoProcessing insilico = new InsilicoProcessing();
            experimental.setMasses(masses);
            experimental.processQuery(experimental.getMasses());
            
            List<InsilicoBacterium> insilico_result = insilico.runRCode(level,experimental.getQuery(), sigma, "alone");
            List<InsilicoBacterium> augmented_result = insilico.runRCode(level,experimental.getQuery(), sigma, "augmented");
            
            ObjectMapper mapper = new ObjectMapper();

            session.getBasicRemote().sendText(mapper.writeValueAsString(ml_result)+"*"+mapper.writeValueAsString(insilico_result)+"*"+mapper.writeValueAsString(augmented_result));            
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *  Method is called when an error occurs.
     *  
     * @param e
     */
	@OnError
	public void onError(Throwable e){
		e.printStackTrace();
	}

}
