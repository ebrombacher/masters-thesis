package webInterface.webInterface;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

import org.renjin.repackaged.guava.base.Joiner;
import org.renjin.script.RenjinScriptEngineFactory;
import org.renjin.sexp.DoubleArrayVector;
import org.renjin.sexp.Vector;

import webInterface.webInterface.ExperimentalBacterium;
import weka.classifiers.Classifier;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils.DataSource;
import de.bwaldvogel.liblinear.SolverType;
 
/**
 * 
 *
 */
public class ExperimentalProcessing {      
		    
	private String masses;
	    
	private  ArrayList<Double> query;
	
    public static void main( String[] args ) throws Exception
    {
    	Locale.setDefault(new Locale("en", "US"));
    	ExperimentalProcessing proc = new ExperimentalProcessing();
        //proc.setMasses("3332;3871;4169;4256;4318;4445;4698;5003;5049;5706;6022;6304;7208;7347;7477;7746;8637;8913;9183;9398;10008");
        proc.setMasses("3000;4444;6666");

    	proc.processQuery(proc.getMasses());

        
        List<ExperimentalBacterium> bact_species = proc.getBacteriaList("species", proc.getQuery());    
        for (ExperimentalBacterium ba:bact_species) {
        	System.out.println(ba.getName());
        	System.out.println(ba.getScore());
        }
    }

  
	private final static Pattern pattern = Pattern.compile("\\s+");

	/**
	 * Get first word
	 * @param s
	 * @return
	 */
	public static String firstWord(String s) {
		return pattern.split(s, 2)[0];
	}
 
	/**
	 * Process query
	 * @throws Exception
	 */
	public void processQuery(String masslist) throws Exception {
		String path = null;
		List<String> masses = new ArrayList<String>();

		String lines[] = masslist.split(";");
		for(String line: lines) {
			masses.add(line);
		}
		
    	Locale.setDefault(new Locale("en", "US"));

		ArrayList<Double> result = new ArrayList<Double>();
		for (int i = 0; i < masses.size(); i++) {
			try {
				Double mass = Double.parseDouble(masses.get(i));
				if (mass>0 && mass<=20000) {
					result.add(mass);
				}
			} catch (NumberFormatException ex) {
				// if is not a number, not our problem!
				System.out.println(masses.get(i)+" not a number"); 
			}
		}
		System.out.println(result);
		this.setQuery(result);
	}
	
	/**
	 * Run analysis
	 * @param rank
	 * @param query2
	 * @return bacteria list
	 * @throws Exception
	 */
	public List<ExperimentalBacterium> getBacteriaList(String rank, ArrayList<Double> query2) throws Exception {
		String finalString = runRCode(rank, query2);

		CSVLoader loader = new CSVLoader();
		InputStream stream = new ByteArrayInputStream(finalString.getBytes(StandardCharsets.UTF_8));

		loader.setSource(stream);
 
		Instances testInstances = loader.getDataSet();
		
		// TO GENERATE MODEL FILES:
		//String modelFile = "/trainingdata_"+rank+"_atleast4_peaks.csv";
		// buildModel(modelFile, rank);
		
		Map<String, Double> map = makePredictions(rank, testInstances);

		List<ExperimentalBacterium> bacteria = new ArrayList<ExperimentalBacterium>();
    	
		for (String key : map.keySet()) { 
			bacteria.add(new ExperimentalBacterium(key, map.get(key)));
		}
		return bacteria;
	}

	/**
	 * Run R code using Renjin
	 * @param rank
	 * @param query2
	 * @return
	 * @throws ScriptException
	 */
	private String runRCode(String rank, ArrayList<Double> query2) throws ScriptException {

		RenjinScriptEngineFactory factory = new RenjinScriptEngineFactory();
		
		// create a Renjin engine:
		ScriptEngine engine = factory.getScriptEngine();

		engine.put("sample", new DoubleArrayVector(query2));
	    
		engine.eval("selected_rank  <- '"+rank+"'");
		engine.eval("approach <- 'peaks'"); 	    
		
		// Read in R script
		InputStream is = getClass().getResourceAsStream("/experimental_model.R");
		Reader reader = new InputStreamReader(is);
		engine.eval(reader);
		
		ArrayList valueList = convertRenjinVectorToList("values", "double", engine);
		valueList.remove(valueList.size() - 1);
		valueList.add("rank");
		ArrayList featureList = convertRenjinVectorToList("features", "string", engine);
	    
		String valueString = Joiner.on(",").join(valueList);

		String featureString = Joiner.on(",").join(featureList);
		String finalString = featureString+"\n"+valueString;
		return finalString;
	}
	
	/**
	 * Define classifier
	 * @param modelFile
	 * @param rank
	 * @throws Exception
	 */
	public void buildModel(String modelFile, String rank) throws Exception {
		DataSource source;

		source = new DataSource(getClass().getResource(modelFile).getPath());

		Instances insts = source.getDataSet();

		insts.setClassIndex(insts.numAttributes() - 1 );   
		LibLINEAR classifier = new LibLINEAR();
		classifier.setSolverType(SolverType.L2R_LR);
		classifier.setProbabilityEstimates(true);
		classifier.setEps(0.01);
		classifier.setCost(10);
		classifier.setBias(1);

		classifier.buildClassifier(insts);
      
		// write model and instances output files
		weka.core.SerializationHelper.write(rank+"_peaksApproach.model", classifier);
		weka.core.SerializationHelper.write(rank+"_peaksApproach_insts", insts);

	}
  
	/**
	 * Make predictions
	 * @param rank
	 * @param testInstances
	 * @return
	 * @throws Exception
	 */
	public Map<String, Double> makePredictions(String rank, Instances testInstances) 
		    throws Exception {
		
		// Mark the last attribute in each instance as the true class.
		testInstances.setClassIndex(testInstances.numAttributes()-1);
		int numTestInstances = testInstances.numInstances();
		   
		// Load classifier and instances from file
		// // Uncomment for peaks approach
		Classifier classifier = (Classifier) weka.core.SerializationHelper.read(getClass().getResourceAsStream("/"+rank+"_peaksApproach.model"));
		Instances insts = (Instances) weka.core.SerializationHelper.read(getClass().getResourceAsStream("/"+rank+"_peaksApproach_insts"));
	
		// // Uncomment for bins approach
		//Classifier classifier = (Classifier) weka.core.SerializationHelper.read(getClass().getResourceAsStream("/"+rank+".model"));
		//Instances insts = (Instances) weka.core.SerializationHelper.read(getClass().getResourceAsStream("/"+rank+"_insts"));
		
		TreeMap<String, Double> map = new TreeMap<>();
		// Loop over each test instance.
		for (int i = 0; i < numTestInstances; i++){
			// Get the prediction probability distribution.
			double[] predictionDistribution = classifier.distributionForInstance(testInstances.instance(i)); 

			// Loop over all the prediction labels in the distribution.
			for (int predictionDistributionIndex = 0; 
					predictionDistributionIndex < predictionDistribution.length; 
					predictionDistributionIndex++) {
				// Get this distribution index's class label.
				String predictionDistributionIndexAsClassLabel = insts.classAttribute().value(predictionDistributionIndex);

				// Get the probability.
				double predictionProbability = 
						predictionDistribution[predictionDistributionIndex];

				map.put(predictionDistributionIndexAsClassLabel.replaceAll("_", " "), round(predictionProbability, 6));
			}
		}
		Map<String, Double> sortedMap = sortByValue(map);
		   
		return sortedMap;
	}

   
	/**
	 * Convert Renjin vector to ArrayList
	 * @param variable
	 * @param datatype
	 * @param engine
	 * @return
	 * @throws ScriptException
	 */
	@SuppressWarnings("rawtypes")
	public ArrayList convertRenjinVectorToList(String variable, String datatype, ScriptEngine engine) throws ScriptException {
		Vector vec =  (Vector)engine.eval(variable);
		ArrayList list = new ArrayList();
		for (int i=0; i<vec.length(); i++) {
			if(datatype.equals("string")) {
				list.add(vec.getElementAsString(i));
			} else {
				list.add(vec.getElementAsString(i));
			}
		}
		return list;
	}
   
	/**
	 * Sort map
	 * @param unsortMap
	 * @return
	 */
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> unsortMap) {

		List<Map.Entry<K, V>> list =
				new LinkedList<Map.Entry<K, V>>(unsortMap.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<K, V>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	} 
   
	/**
	 * Round double
	 * @param value
	 * @param places
	 * @return
	 */
	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}


	public String getMasses() {
		return masses;
	}


	public void setMasses(String masses) {
		this.masses = masses;
	}


	public ArrayList<Double> getQuery() {
		return query;
	}


	public void setQuery(ArrayList<Double> query) {
		this.query = query;
	}
   
}