package webInterface.webInterface;

public class ExperimentalBacterium  {

	String name;  
	Double score;

	public ExperimentalBacterium(){
		this.name = new String();  
		this.score = new Double(0);
	}

	public ExperimentalBacterium(String name, Double score){
		this.name = name;  
		this.score = score; 
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name; 
	}

	public double getScore() {
		return this.score;
	}

	public void setScore(double score) {
		this.score = score;
	}

}