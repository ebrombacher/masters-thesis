package webInterface.webInterface;

public class ProteinInfo {
	String protein;
	Double mass;
	Double queryMass;
	
	public ProteinInfo(){
		this.protein = new String();  
		this.mass = new Double(0);
		this.queryMass = new Double(0);
	}

	public ProteinInfo(String protein, Double mass, Double queryMass){
		this.protein = protein;  
		this.mass = mass; 
		this.queryMass = queryMass;
	}
	
	public String getProtein() {
		return this.protein;
	}

	public void setProtein(String protein) {
		this.protein = protein; 
	}
	
	public Double getMass() {
		return this.mass;
	}

	public void setMass(Double mass) {
		this.mass = mass; 
	}
	
	public Double getQueryMass() {
		return this.queryMass;
	}

	public void setQueryMass(Double queryMass) {
		this.queryMass = queryMass; 
	}
}
