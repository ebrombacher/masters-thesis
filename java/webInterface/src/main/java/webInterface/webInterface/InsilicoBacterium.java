package webInterface.webInterface;

import java.util.ArrayList;

public class InsilicoBacterium  {

	String name;  
	Double score;
	ArrayList<ProteinInfo> proteinInfo;

	public InsilicoBacterium(){
		this.name = new String();  
		this.score = new Double(0);
		this.proteinInfo = new ArrayList<ProteinInfo>();
	}

	public InsilicoBacterium(String name, Double score, ArrayList<ProteinInfo> proteinInfo){
		this.name = name;  
		this.score = score; 
		this.proteinInfo = proteinInfo;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name; 
	}

	public double getScore() {
		return this.score;
	}

	public void setScore(double score) {
		this.score = score;
	}
	
	public ArrayList<ProteinInfo> getProteinInfo() {
		return this.proteinInfo;
	}

	public void setProteinInfo (ArrayList<ProteinInfo> proteinInfo) {
		this.proteinInfo = proteinInfo;
	}
}