package webInterface.webInterface;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

import org.renjin.script.RenjinScriptEngineFactory;
import org.renjin.sexp.Vector;

import com.fasterxml.jackson.databind.ObjectMapper;

public class InsilicoProcessing  {
	
	public static void main( String[] args ) throws Exception {
		InsilicoProcessing map = new InsilicoProcessing();
		ExperimentalProcessing proc = new ExperimentalProcessing();
        proc.setMasses("3000;4444;6666;10000;12000;7777;4444");
        proc.processQuery(proc.getMasses());
        
        Integer sigma = 3;
        List<InsilicoBacterium> finalString = map.runRCode("genus",proc.getQuery(), sigma, "augmented");
		//System.out.println(finalString);
		
		for (InsilicoBacterium bact: finalString){
			System.out.println(bact.getName());
			System.out.println(bact.getScore());
			for (ProteinInfo info: bact.getProteinInfo()){
				System.out.println(info.getProtein());
				System.out.println(info.getMass());
				System.out.println(info.getQueryMass());
			}
		}
		
        ObjectMapper mapper = new ObjectMapper();

		System.out.println(mapper.writeValueAsString(finalString));
    }
		
	/**
	 * Run R code using Renjin
	 * @param rank
	 * @param query2
	 * @return bacteria list with scores and masses matched to query masses
	 * @throws ScriptException
	 */
	public List<InsilicoBacterium> runRCode(String rank, ArrayList<Double> query2, Integer sigma, String mode) throws Exception {
		RenjinScriptEngineFactory factory = new RenjinScriptEngineFactory();
		
		// create a Renjin engine:
		ScriptEngine engine = factory.getScriptEngine();
	 
		engine.put("sigma", sigma);
		engine.put("mode", mode);
		engine.eval("precision <- 'precision1'"); 	
		engine.eval("level <- '"+rank+"'");
			
		String pathString = "naiveBayes_species_precision1.csv";
		ClassLoader classLoader = getClass().getClassLoader();
		String path = classLoader.getResource(pathString).getFile();
		engine.eval("path <- '"+path+"'");
		
		String pathString2 = "naiveBayes_species_precision1_PTMsRemoved.csv";
		String path2 = classLoader.getResource(pathString2).getFile();
		engine.eval("path2 <- '"+path2+"'");
		
		String freqString = "kegg_species_frequencies.csv";
		String freq = classLoader.getResource(freqString).getFile();
		engine.eval("freq <- '"+freq+"'");
		
		String expString = "expDataframes_combined.csv";
		String exp_df = classLoader.getResource(expString).getFile();
		engine.eval("exp_df <- '"+exp_df+"'");
				

		engine.put("query", query2);
		
		// Read in R script
		InputStream is = getClass().getResourceAsStream("/insilico_model.R");
		Reader reader = new InputStreamReader(is);
		engine.eval(reader);
		ArrayList nameList = convertRenjinVectorToList("bacterium", "string", engine);
		ArrayList scoreList = convertRenjinVectorToList("result", "double", engine);
		
		ArrayList proteinsList = convertRenjinVectorToList("proteins", "string", engine);
		ArrayList massesList = convertRenjinVectorToList("mass", "string", engine);
		ArrayList queryMassesList = convertRenjinVectorToList("query_mass", "string", engine);
		
		List<InsilicoBacterium> mapBacteriaList = new ArrayList<InsilicoBacterium>(nameList.size());
		
		for (int i = 0; i < nameList.size()-1; i++) {
			String proteins = (String) proteinsList.get(i);
			String masses = (String) massesList.get(i);
			String queryMasses = (String) queryMassesList.get(i);
			
			List<String> proteinsSplit = new ArrayList<String>();
			List<String> massesSplit = new ArrayList<String>();
			List<String> queryMassesSplit = new ArrayList<String>();
			
			if (proteins != null) {
				proteinsSplit = Arrays.asList(proteins.split(","));
				massesSplit = Arrays.asList(masses.split(","));
				queryMassesSplit = Arrays.asList(queryMasses.split(","));
			}
			
			ArrayList<ProteinInfo> infoList = new ArrayList<ProteinInfo> ();
			for (int j = 0; j < proteinsSplit.size(); j++) {
				ProteinInfo info = new ProteinInfo(proteinsSplit.get(j), Double.parseDouble(massesSplit.get(j)), Double.parseDouble(queryMassesSplit.get(j)));
				infoList.add(info);

			}

			InsilicoBacterium bact = new InsilicoBacterium(String.valueOf(nameList.get(i)), Double.valueOf(scoreList.get(i).toString().replace(",",".")), infoList);
			mapBacteriaList.add(bact);
		}	    
		return mapBacteriaList;
	}

	/**
	 * Convert Renjin vector to ArrayList
	 * @param variable
	 * @param datatype
	 * @param engine
	 * @return
	 * @throws ScriptException
	 */
	@SuppressWarnings("rawtypes")
	public ArrayList convertRenjinVectorToList(String variable, String datatype, ScriptEngine engine) throws ScriptException {
		Vector vec =  (Vector)engine.eval(variable);
		ArrayList list = new ArrayList();
		for (int i=0; i<vec.length(); i++) {
			if(datatype.equals("string")) {
				list.add(vec.getElementAsString(i));
			} else {
				list.add(vec.getElementAsString(i));
			}
		}
		return list;
	}
}