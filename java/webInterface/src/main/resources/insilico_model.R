

probab <- function(p, std_dev, mean){
  prob <- (1/(std_dev*sqrt(2*pi)))*exp(-((p-mean)^2/(2*std_dev^2)))
  return(prob)
}


generateMap <- function(query, sigma, level, precision, path, path2, freq, exp_df, combineExp){

  df_database <- rbind(read.csv(path,header=T), read.csv(path2,header=T))
  
  # Remove duplicate rows resulting from combining the data frames
  df_database <- unique(df_database)

  df_database <- read.csv(path,header=T)
	
  exp_df <- read.csv(exp_df)

  if (combineExp){
  	# remove row in silico database if df_database$class is not in experimental database
  	#df_database<-df_database[df_database$class %in% colnames(exp_df),]
  	df_database <- df_database[is.element(df_database$class, colnames(exp_df)),]
  }
  
  df_database$keep <- FALSE
  
  map <- data.frame(matrix(nrow = length(unique(df_database$class)), ncol = length(unique(df_database$protein))))
  colnames(map) <- unique(df_database$protein)
  rownames(map) <- unique(df_database$class)
  
  none <- 6.049268112978591*10^(-6)
  
  map[is.na(map)] <- none
  
  
  for (p in query){
    df_database$keep[(df_database$mean<=(p+sigma))&(df_database$mean>=(p-sigma))]=TRUE
  }
  
  df_database <- df_database[df_database$keep==TRUE,]
  
  masses  <- rev(expand.grid(list(bacterium=unique(df_database$class),proteins=unique(df_database$protein))))
  namevector <- c("mass", "query_mass", "probab")
  masses[ , namevector] <- NA
  
  if (nrow(df_database)>0){
    df_database$keep <- NULL
  
    for (p in query){
      # go through each row of db 
      for(i in 1:nrow(df_database)) { 
  
        #get species, protein, std_dev, mean
        class <- df_database[i,1]
        protein <- df_database[i,2]
        std_dev <- df_database[i,4]
        mean <- df_database[i,6]
      
        # calculate probability
        new <- probab(p, std_dev, mean)
        
        if (combineExp){
            # check if mass "p" occurs for value of "class" in any entry of the respective class of the experimental datasets with a
            # tolerance of 10 Da and penalize with 20% reduction of "new" if not
            reward <- 10
            # if(class %in% colnames(exp_df)){
            if(is.element(class, colnames(exp_df))){
              idx <- which(colnames(exp_df)==class)
              found = FALSE
              for (index in idx){
                #expMasses <- as.vector(na.omit(exp_df[,index]))
                
                expMasses <- as.numeric(as.character(exp_df[,index]))
                expMasses <- as.vector(na.omit(expMasses))
                for (m in expMasses){
                  tol <- 30 
                  if (abs(m-p)<tol){
                    found = TRUE 
                    break
                  }
                }
                if (found){break}
              }
              if (found) {new <- reward*new}
            }
          }
        
        if(new>map[as.character(class), as.character(protein)]){
          map[as.character(class), as.character(protein)] <- new
          masses$mass[masses$bacterium==as.character(class)&masses$proteins==as.character(protein)] <- mean
          masses$query_mass[masses$bacterium==as.character(class)&masses$proteins==as.character(protein)] <- p
          masses$probab[masses$bacterium==as.character(class)&masses$proteins==as.character(protein)] <- new
        }
      }
    }
  }  
  map <- transform(map, prod=Reduce(`*`, map))
  
  masses <- masses[!is.na(masses$mass),]
  
  # if multiple rows with same bacterium and query_mass keep row with highest value of probab
  masses <- masses[order(masses$bacterium, masses$query_mass, -abs(masses$probab) ), ] ### sort first
  masses <- masses[!duplicated(masses[,c("bacterium","query_mass")]),]
  masses$probab <- NULL
  
  masses <- masses[with(masses, order(bacterium, mass)),]
  masses <- data.frame(lapply(masses, as.character), stringsAsFactors=FALSE)

  masses <- aggregate(. ~ bacterium, data = masses, paste, collapse = ",")
  
    
  freq <- read.csv(freq)
  freq$bacterium <- gsub("\\ ", "_", freq$bacterium)
  
  # total no. of samples
  total <- sum(freq$frequency)
  freq$proportion <- freq$frequency/total
  map$bacterium <- rownames(map)
  
  library(plyr)
  final_map <- join(map,freq,by='bacterium')
  final_map$result <- final_map$prod*final_map$proportion
  
  
  final_map <- join(final_map,masses,by='bacterium')
  
  if (level == "genus"){
  	final_map$bacterium <- sub("_.*", "", final_map$bacterium)
  	final_map <- final_map[order(final_map$bacterium, -final_map$result ), ] ### sort first
  	final_map <- final_map[ !duplicated(final_map$bacterium), ]  ### Keep highest
  }

  final_map <- final_map[order(-final_map$result),] 
  return(final_map)
}

if (mode == "alone"){
	map <- generateMap(query, sigma, level, precision, path, path2, freq, exp_df, FALSE)
} else if (mode == "augmented"){
	map <- generateMap(query, sigma, level, precision, path, path2, freq, exp_df, TRUE)
}

bacterium <- map$bacterium
result <- round(-log(map$result), digits = 2)
proteins <- map$proteins
mass <- map$mass
query_mass <- map$query_mass

